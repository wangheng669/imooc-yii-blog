<?php
return [
    'Blog' => '博客',
    'Home' => '首页',
    'About' => '关于',
    'Contact' => '联系',
    'Signup' => '注册',
    'Login' => '登录',
    'Logout' => '退出',
];
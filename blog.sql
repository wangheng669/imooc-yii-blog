/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : blog

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 10/12/2019 00:28:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '自动登录key',
  `password_hash` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '加密密码',
  `password_reset_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重置密码token',
  `email_validate_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱验证token',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `role` smallint(6) NOT NULL DEFAULT 10 COMMENT '角色等级',
  `status` smallint(6) NOT NULL DEFAULT 10 COMMENT '状态',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `vip_lv` int(11) NULL DEFAULT 0 COMMENT 'vip等级',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', 'Tp8kng0Jn5-TZskC0SOwDtA2SIV6ooNp', '$2y$13$zv4eXwX3fKwsgLEDi185euBsjmAFvEJt39QmvaaHkOCKUUm.Z/M/u', '', NULL, '2658803113@qq.com', 10, 10, NULL, 0, 1575706614, 1575706614, 'YcR6V3jI_-JhOlmA2v7cfOhgc5yXHJ2__1575706614');
INSERT INTO `admin` VALUES (2, 'test', 'R31XyjECiR0n3AyJBrRxIvC4CDczbMjo', '$2y$13$zQ1v3tGjGupfEOpbzPv9veXpfHMpz.0W0rKmn9MGwml/W3t1zZUpO', NULL, NULL, 'wangheng@qq.com', 10, 10, NULL, 0, 1575706673, 1575706673, '9wv5xBdS_v9Fnah6Gv_VBRY4SalJ6fmm_1575706673');

-- ----------------------------
-- Table structure for cats
-- ----------------------------
DROP TABLE IF EXISTS `cats`;
CREATE TABLE `cats`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `cat_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cats
-- ----------------------------
INSERT INTO `cats` VALUES (1, '分类1');
INSERT INTO `cats` VALUES (2, '分类2');

-- ----------------------------
-- Table structure for post_extends
-- ----------------------------
DROP TABLE IF EXISTS `post_extends`;
CREATE TABLE `post_extends`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `post_id` int(11) NULL DEFAULT NULL COMMENT '文章id',
  `browser` int(11) NULL DEFAULT 0 COMMENT '浏览量',
  `collect` int(11) NULL DEFAULT 0 COMMENT '收藏量',
  `praise` int(11) NULL DEFAULT 0 COMMENT '点赞',
  `comment` int(11) NULL DEFAULT 0 COMMENT '评论',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章扩展表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of post_extends
-- ----------------------------
INSERT INTO `post_extends` VALUES (38, 68, 15, 0, 0, 0);

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `summary` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '摘要',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `label_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签图',
  `cat_id` int(11) NULL DEFAULT NULL COMMENT '分类id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `is_valid` tinyint(1) NULL DEFAULT 0 COMMENT '是否有效：0-未发布 1-已发布',
  `created_at` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_cat_valid`(`cat_id`, `is_valid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (4, '测试测试测试测试测试', '测试测试测试测试测试测试', '<p>测试测试测试测试测试测试</p>', '/image/20191208/1575782495546776.jpg', 1, 3, 'hhhhhhh', 1, 1575782538, 1575782538);
INSERT INTO `posts` VALUES (23, '测试测试测试测试测试', '测试测试测试测试测试', '<p>测试测试测试测试测试</p>', '', 1, 3, 'hhhhhhh', 1, 1575815126, 1575815126);
INSERT INTO `posts` VALUES (24, '测试测试测试测试测试', '测试测试测试测试测试', '<p>测试测试测试测试测试</p>', '', 0, 3, 'hhhhhhh', 1, 1575815150, 1575815150);
INSERT INTO `posts` VALUES (25, '测试测试测试测试测试', '测试测试测试测试测试测试测试', '<p>测试测试测试测试测试测试测试</p>', '', 0, 3, 'hhhhhhh', 1, 1575815208, 1575815208);
INSERT INTO `posts` VALUES (29, '测试测试测试测试测试', '测试测试测试测试测试', '<p>测试测试测试测试测试</p>', '', 0, 3, 'hhhhhhh', 1, 1575815513, 1575815513);
INSERT INTO `posts` VALUES (45, '测试测试测试测试测试', '测试测试测试测试测试', '<p>测试测试测试测试测试</p>', '', 1, 3, 'hhhhhhh', 1, 1575816278, 1575816278);
INSERT INTO `posts` VALUES (46, '测试测试测试测试测试', '测测测而出', '<p>测测测而出</p>', '', 0, 3, 'hhhhhhh', 1, 1575816318, 1575816318);
INSERT INTO `posts` VALUES (52, '测试测试测试测试测试', '测测测而出', '<p>测测测而出</p>', '', 0, 3, 'hhhhhhh', 1, 1575817254, 1575817254);
INSERT INTO `posts` VALUES (67, '测试测试测试测试测试', '测试测试测试测试测试', '<p>测试测试测试测试测试</p>', '', 1, 3, 'hhhhhhh', 1, 1575819605, 1575819605);
INSERT INTO `posts` VALUES (68, '测试测试测试测试测试', '测试测试测试测试测试', '<p>测试测试测试测试测试</p>', '', 0, NULL, NULL, 1, 1575820122, 1575820122);

-- ----------------------------
-- Table structure for relation_post_tags
-- ----------------------------
DROP TABLE IF EXISTS `relation_post_tags`;
CREATE TABLE `relation_post_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `post_id` int(11) NULL DEFAULT NULL COMMENT '文章ID',
  `tag_id` int(11) NULL DEFAULT NULL COMMENT '标签ID',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `post_id`(`post_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章和标签关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of relation_post_tags
-- ----------------------------
INSERT INTO `relation_post_tags` VALUES (1, 67, NULL);
INSERT INTO `relation_post_tags` VALUES (2, 68, 2);

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `tag_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签名称',
  `post_num` int(11) NULL DEFAULT 0 COMMENT '关联文章数',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tag_name`(`tag_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tags
-- ----------------------------
INSERT INTO `tags` VALUES (2, 'ces', 2);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '自动登录key',
  `password_hash` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '加密密码',
  `password_reset_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重置密码token',
  `email_validate_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱验证token',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `role` smallint(6) NOT NULL DEFAULT 10 COMMENT '角色等级',
  `status` smallint(6) NOT NULL DEFAULT 10 COMMENT '状态',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `vip_lv` int(11) NULL DEFAULT 0 COMMENT 'vip等级',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'wangheng669', 'Tp8kng0Jn5-TZskC0SOwDtA2SIV6ooNp', '$2y$13$zv4eXwX3fKwsgLEDi185euBsjmAFvEJt39QmvaaHkOCKUUm.Z/M/u', '', NULL, '2658803113@qq.com', 10, 10, NULL, 0, 1575706614, 1575706614, 'YcR6V3jI_-JhOlmA2v7cfOhgc5yXHJ2__1575706614');
INSERT INTO `user` VALUES (2, 'wangheng', 'R31XyjECiR0n3AyJBrRxIvC4CDczbMjo', '$2y$13$zQ1v3tGjGupfEOpbzPv9veXpfHMpz.0W0rKmn9MGwml/W3t1zZUpO', NULL, NULL, 'wangheng@qq.com', 10, 10, NULL, 0, 1575706673, 1575706673, '9wv5xBdS_v9Fnah6Gv_VBRY4SalJ6fmm_1575706673');
INSERT INTO `user` VALUES (3, 'hhhhhhh', 'Hk7Jh3nLq9OrUSbQqSL7Wv08RkST406P', '$2y$13$7FrZfX46GR0rfEUve5NsHuZpwdXPIUv7m5btA.IJkmGlf6CJsIB9i', NULL, NULL, 'hhhhhhh@qq.com', 10, 10, NULL, 0, 1575719754, 1575719754, 'nBxD-HQNBHug1ttYfREUNtzuV8ZR0JDU_1575719754');

SET FOREIGN_KEY_CHECKS = 1;

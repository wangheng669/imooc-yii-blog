<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property int $id 自增ID
 * @property string|null $title 标题
 * @property string|null $summary 摘要
 * @property string|null $content 内容
 * @property string|null $label_img 标签图
 * @property int|null $cat_id 分类id
 * @property int|null $user_id 用户id
 * @property string|null $user_name 用户名
 * @property int|null $is_valid 是否有效：0-未发布 1-已发布
 * @property int|null $created_at 创建时间
 * @property int|null $updated_at 更新时间
 */
class PostModel extends BaseModel
{

    const IS_VALID = 1;
    const NO_VALID = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['cat_id', 'user_id', 'is_valid', 'created_at', 'updated_at'], 'integer'],
            [['title', 'summary', 'label_img', 'user_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '标题',
            'summary' => '简介',
            'content' => '内容',
            'label_img' => '表情图',
            'cat_id' => '分类',
            'cat.cat_name' => '分类',
            'user_id' => '用户ID',
            'user_name' => '用户名',
            'is_valid' => '状态',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    public function getRelate()
    {
        return $this->hasMany(RelationPostTagsModel::className(),['post_id' => 'id']);
    }

    public function getExtend()
    {
        return $this->hasOne(PostExtendModel::className(),['post_id' => 'id']);
    }

    public function getCat()
    {
        return $this->hasOne(CatModel::className(),['id' => 'cat_id']);
    }

}

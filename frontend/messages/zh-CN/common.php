<?php
return [
    'Blog' => '博客',
    'Home' => '首页',
    'About' => '关于',
    'Contact' => '联系',
    'Signup' => '注册',
    'Login' => '登录',
    'Logout' => '退出',
    'This username has already been taken.' => '该用户名已存在',
    'This email address has already been taken.' => '该邮箱已存在',
    'Two Times the password is not consitent.' => '两次密码输入不一致',
];